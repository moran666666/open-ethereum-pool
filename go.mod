module gitee.com/moran666666/open-ethereum-pool

go 1.16

require (
	github.com/garyburd/redigo v1.6.3 // indirect
	github.com/gorilla/mux v1.8.0
	github.com/ubiq/go-ubiq/v5 v5.3.1
	github.com/ubiq/ubqhash v1.0.0
	github.com/yvasiyarov/go-metrics v0.0.0-20150112132944-c25f46c4b940 // indirect
	github.com/yvasiyarov/gorelic v0.0.7
	github.com/yvasiyarov/newrelic_platform_go v0.0.0-20160601141957-9c099fbc30e9 // indirect
	gopkg.in/bsm/ratelimit.v1 v1.0.0-20160220154919-db14e161995a // indirect
	gopkg.in/redis.v3 v3.6.4
)
