# This Makefile is meant to be used by people that do not usually work
# with Go source code. If you know what GOPATH is then you probably
# don't need to bother with make.

.PHONY: all clean

all:
	go build -o open-ethereum-pool main.go

clean:
	rm -fr open-ethereum-pool
